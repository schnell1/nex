nex might become a CMS for parameter profiles, e.g. nutritional profiles for foods.

# Status
- not even alpha
- personal experiment
- outlining ideas
- structuring code
- writing documentation

# Contribution
- create a fork
- look into `examples/nutri/` and get familiar with the interface
- execute `cargo doc` and open documentation in `target/doc/nex/index.html`
- execute `grep -Rni todo` and find places where help is needed
- create an issue, describe your changes and send a link to your code
- thx <3

# Current focus
- application definition:
  - defining TOML input
  - parsing input
- PostgreSQL query generation for database setup and accessing data
- developing services, templating HTML forms, validation for:
    - filling profiles
    - filling items
    - localizing data
    - localizing app
- command line interface (CLI) with subcommands for `nex`:
    - `app check`
    - `data setup`
    - `app build`
    - `app run`
    - (`app migrate`)
    - (`data migrate`)
    - (`app dev`)
- configurable endpoints
- find auth solution
- database-level role system for granular access to content and app

# Using nex: Creating a data application
1. Define the application in `nex.toml` file in a new folder.
2. Private runtime configuration is stored in `.env` file or as environmental variables.
3. ?
4. Execute CLI commands: `nex db setup` and then `nex app run`.

# Requirements: Software
- Rust nightly and cargo
- Postgres server

# Application definition: Example
```toml
[app]
name="nutri"
roles=["visitor", "root"]

[item.foods]
name_one="food"
profiles=["nutritional"]

[profile.nutritional]
per_either=[{factor="1/100", unit="g"}, {factor="1/100", unit="ml"}]
params=[
	{ group="basic_info", params=[
		{ name="energy", unit="kJ" },
		{ name="fats",  unit="g", sum_of=["saturated_fats", "unsaturated_fats"]},
		{ name="saturated_fats", unit="g" },
		{ name="unsaturated_fats", unit="g" },
		{ name="carbohydrates", unit="g" },
	]},
]
```

# License
Public Domain.
