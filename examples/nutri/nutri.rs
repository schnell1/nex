//! nutri - managing nutritional profiles for foods

#![feature(duration_constants)]

use nex::{App, AppResult, Data, Server};
use tokio;

#[tokio::main]
pub async fn main() -> AppResult {
    // Parse files `.env` and `nex.toml` to create `App`
    let app = App::try_from_env_and_definition_file("nex.toml")?;
    // Setup database from `AppDefinition`
    App.data_setup()?;

    // Overwrite refresh interval for data.
    let seconds = std::time::Duration::SECOND * 30;
    app.data_refresh_with_interval(seconds);

    // Run server.
    (app, data).into::<Server>().run().await?
}
