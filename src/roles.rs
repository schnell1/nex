//! Roles for granular access to services and data.

use enum_iterator::IntoEnumIterator;
use enumset::{EnumSet, EnumSetType};
use parse_display::{Display, FromStr};
use serde::Deserialize;
use std::collections::HashSet;

/// Sum type of available roles.
#[rustfmt::skip]
#[derive(Deserialize, Display, FromStr, Debug, IntoEnumIterator, EnumSetType, Hash)]
pub enum Role {
    /// Read published content and read published comments 
    Visitor,
    /// Read/create/edit their un/published comments
    Curator,
    /// Read/create/edit their un/published content
    Creator,
    /// Read/create/edit localized content by author permission
    LocalizerData,
    /// Read/create/edit app localization and nex localization
    LocalizerApp,
    /// Read/edit content by permission
    Editor,
    /// Read/create/edit all content
    AdminData,
    /// Read/create/edit application settings
    AdminApp,
    /// Read/create/edit everything
    Root,
}

/// Set of roles
pub type Roles = EnumSet<Role>;
