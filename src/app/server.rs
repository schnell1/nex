//! Server as a web interface for humans.

use super::App;
use crate::Data;

use futures::FutureExt;
use hyper::{service::Service as HyperService, Request, Response, StatusCode};
use std::{
    future::Future,
    pin::Pin,
    sync::Weak,
    task::{Context, Poll},
    time::Duration,
};

use anyhow;
use thiserror;
/// Errors from `Server`.
#[derive(thiserror::Error, Debug)]
#[non_exhaustive]
pub enum Error {
    #[error(transparent)]
    Http(#[from] http::Error),
    #[error(transparent)]
    Other(#[from] anyhow::Error),
}

/// Server ready to launch from parent `Weak<App>`.
#[derive(Debug, Clone)]
pub struct Server {
    app: Weak<App>,
}

impl Server {
    pub async fn run() -> Result<(), Error> {
        todo!()
    }
}

impl From<Weak<App>> for Server {
    fn from(app: Weak<App>) -> Self {
        Self { app: app.clone() }
    }
}

impl HyperService<Vec<u8>> for Server {
    type Response = Response<Vec<u8>>;
    type Error = Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Vec<u8>) -> Self::Future {
        todo!()
        /*
        // create the body
        let body: Vec<u8> = "hello, world!\n"
            .as_bytes()
            .to_owned();
        // Create the HTTP response
        let resp = Response::builder()
            .status(StatusCode::OK)
            .body(body)
            .expect("Unable to create `http::Response`");

        // create a response in a future.
        let fut = async {
            Ok(resp)
        };

        // Return the response as an immediate future
        Box::pin(fut)
        */
    }
}
