//! Types to describe app definition from `nex.toml`.

use serde_derive::{Deserialize, Serialize};
use serde_json;
use std::{
    collections::{HashMap, HashSet},
    convert::TryInto,
    fs::File,
    sync::{Arc, Weak},
};

use anyhow;
use thiserror;
#[derive(thiserror::Error, Debug)]
#[non_exhaustive]
/// Errors while opening, parsing and validating `nex.toml`.
pub enum Error {
    #[error(transparent)]
    Other(#[from] anyhow::Error),
}

/// Main interface for parsing and validating app definition.
#[derive(Deserialize, Serialize, Default, Debug, Clone)]
pub struct Definition {
    app: App,
    dependencies: Dependencies,
    taxonomies: Vec<Arc<Taxonomy>>,
    items: Vec<Item>,
    profiles: Vec<Arc<Profile>>,
}

impl Definition {
    /*pub fn with_base_file<S: AsRef<str>>(base_file_path: S) -> Result<Self, ConfigError> {
        let file = BaseFile::with_name(base_file_path);

        let mut definition = Config::default();
        definition.merge(file)?;
        // TODO parse correctly
        definition.try_into()
    }*/

    /// Reads and parses `Definition` from `/path/nex.toml`.
    pub fn try_from_file<S: AsRef<str>>(file_path: S) -> Result<Self, Error> {
        todo!()
    }
}

/// `[app]`
#[derive(Deserialize, Serialize, Default, Debug, Clone)]
pub struct App {
    name: String,
    roles: Vec<String>,
    slugs_max_length: usize,
    db_embed: bool,
}

/// `[dependencies]`
#[derive(Deserialize, Serialize, Default, Debug, Clone)]
pub struct Dependencies {
    // TODO: instead, forward to Cargo's dependency solver
    rust: Vec<String>,
}

/// `[item.NAME]`
#[derive(Deserialize, Serialize, Default, Debug, Clone)]
pub struct Item {
    name: String,
    single: String,
    localized: Localized,
    slugs: Slugs,
    taxonomies: Vec<Weak<Taxonomy>>,
    images: Images,
    texts: Vec<Text>,
    profiles: Vec<Weak<Profile>>,
}

/// `images = ...`
#[derive(Deserialize, Serialize, Debug, Clone)]
pub enum Images {
    Localized(Localized),
    Bool(bool),
}

impl Default for Images {
    fn default() -> Self {
        Self::Bool(false)
    }
}

/// `slugs = { amount, localized }`
#[derive(Deserialize, Serialize, Default, Debug, Clone)]
pub struct Slugs {
    amount: Amount,
    localized: Localized,
}

/// `[taxonomy.NAME]`
#[derive(Deserialize, Serialize, Default, Debug, Clone)]
pub struct Taxonomy {
    name_one: String,
    name_many: String,
    localized: Localized,
    // hierarchy: Hierarchy,
    texts: Vec<Text>,
}

/// `{ name = "name", ... }`
#[derive(Deserialize, Serialize, Default, Debug, Clone)]
pub struct Text {
    name: String,
    required: bool,
    localized: Localized,
}

/// `[profile.NAME]`
#[derive(Deserialize, Serialize, Default, Debug, Clone)]
pub struct Profile {
    name_many: String,
    per_either: Vec<PerEither>,
    groups: Vec<Group>,
    params: Vec<Param>,
}

/// `localized =` `MaybeAssigned`
pub type Localized = MaybeAssigned;

/// `... = ` `false` or `"inherit"` or `"unassigned"` or `true` or`"at_least_for_any"` or`"exactly_for_any"` or `"at_least_for_default"` or `"exactly_for_default"`
#[derive(Deserialize, Serialize, Debug, Clone)]
pub enum MaybeAssigned {
    /// Default if omitted.
    /// `inherit`.
    Inherit,
    /// Default if not inheritable.
    /// Unassigned to anything.
    /// `false` or `"unassigned"`.
    Unassigned,
    /// Require at least one version.
    /// `true` or `"at_least_for_any"`.
    AtLeastForAny,
    /// Require exactly one assigned version.
    /// `exactly_for_any`
    ExactlyForAny,
    /// Require at least one version for default and maybe more versions.
    /// `"at_least_for_default"`.
    AtLeastForDefault,
    /// Require exactly one version for default and disallow more versions.
    /// `"exactly_for_default"`.
    ExactlyForDefault,
}

impl Default for MaybeAssigned {
    fn default() -> Self {
        Self::AtLeastForAny
    }
}

/// `amount = ` `"one"` or `"many"`
#[derive(Deserialize, Serialize, Debug, Clone)]
pub enum Amount {
    One,
    Many,
}

impl Default for Amount {
    fn default() -> Self {
        Self::One
    }
}

/// `per_either = { factor = "1/100", unit="g" }`
#[derive(Deserialize, Serialize, Default, Debug, Clone)]
pub struct PerEither {
    factor: String,
    unit: String,
}

/// `group = "name", params = [ { param... }, ]`
#[derive(Deserialize, Serialize, Default, Debug, Clone)]
pub struct Group {
    name: String,
    params: Vec<Param>,
}

/// `{ name = "name", unit = "g", sum_of = ["param", ... ] }`
#[derive(Deserialize, Serialize, Default, Debug, Clone)]
pub struct Param {
    name: String,
    unit: String,
    sum_of: Vec<Param>,
    groups: Vec<Group>,
}
