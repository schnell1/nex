//! Runtime configuration for app is read from environmental variables.

use super::{EndpointsServices, ServicesEndpoints};

use dashmap::DashMap;
use humantime::parse_duration;
use serde::Deserialize;
use std::{collections::HashMap, convert::TryFrom, io, path::PathBuf, time::Duration};
use url::Url;
use zenv::Zenv;

use anyhow;
use thiserror;
#[derive(thiserror::Error, Debug)]
#[non_exhaustive]
pub enum Error {
    #[error(transparent)]
    Io(#[from] io::Error),
}

/// Parses environmental variables and .env file.
#[derive(Clone, Debug, Deserialize)]
pub struct RuntimeConfig {
    pub services_endpoints: ServicesEndpoints,
    pub files_dir: PathBuf,
    pub database_url: Option<Url>,
    pub data_prefix: String,
    pub data_refresh_interval: Duration,
}

/*ENDPOINT_PUBLIC="http+https+http2://0.0.0.0:4000"
ENDPOINT_EDITOR="http2://0.0.0.0:4100/editor"
ENDPOINT_SETTINGS="http2://0.0.0.0:4100/settings"
DATABASE_URL="postgresql://postgres:postgres@localhost/nutri"
DATA_REFRESH_INTERVAL=30s
DATA_PREFIX=""
FILES_DIR="./files"
TLS_CERT="tls/cert.pem"
TLS_KEY="tls/key.rsa"
LOG_LEVEL="debug"

#[derive(Deserialize, Debug)]
struct RuntimeConfigParse {
    endpoints_public
}*/

impl RuntimeConfig {
    pub fn try_from_file<S: AsRef<str>>(dotenv_file_path: S) -> Result<Self, Error> {
        let vars = Zenv::new(dotenv_file_path.as_ref(), false).parse()?;
        Self::try_from(vars)
    }
}

/// Zenv::parse(&self) -> Result<HashMap<String, String>>
impl TryFrom<HashMap<String, String>> for RuntimeConfig {
    type Error = Error;

    fn try_from(args: HashMap<String, String>) -> Result<Self, Self::Error> {
        todo!()
    }
}

impl Default for RuntimeConfig {
    /// data_refresh_interval is carefully set to 60 seconds.
    fn default() -> Self {
        Self {
            services_endpoints: DashMap::new(),
            files_dir: PathBuf::new(),
            database_url: None,
            data_prefix: "".into(),
            data_refresh_interval: Duration::new(60, 0),
        }
    }
}

impl From<&RuntimeConfig> for EndpointsServices {
    fn from(runtime_config: &RuntimeConfig) -> Self {
        Self::from(&runtime_config.services_endpoints)
    }
}
