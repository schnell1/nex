//! Managing endpoints and their connected services.

use super::{RuntimeConfig, Service, Services};

use anyhow::Result;
use dashmap::{DashMap, DashSet};
use enum_iterator::IntoEnumIterator;
use enumset::{EnumSet, EnumSetType};
use parse_display::{Display, FromStr};
use serde::Deserialize;
use std::{
    collections::HashMap,
    net::{IpAddr, SocketAddr},
    str::FromStr,
    sync::Arc,
};
use url::{
    Host::{Ipv4, Ipv6},
    Url,
};

/// Available protocols.
#[derive(Debug, Deserialize, FromStr, EnumSetType)]
pub enum Scheme {
    Http,
    Https,
    Http2,
}

use thiserror;
/// Errors while parsing `Endpoint`
#[derive(thiserror::Error, Debug)]
#[non_exhaustive]
pub enum Error {
    #[error(transparent)]
    Io(#[from] anyhow::Error),
}

pub type Schemes = EnumSet<Scheme>;

/// Secure protocols: HTTPS and HTTP2.
pub fn schemes_default_secure() -> Schemes {
    let mut schemes = Schemes::new();
    schemes.insert(Scheme::Https);
    schemes.insert(Scheme::Http2);
    schemes
}

/// Contains bind parameters and base path.
#[derive(Deserialize, Clone, Debug, PartialEq, Hash, Eq)]
pub struct Endpoint {
    pub socket_addr: SocketAddr,
    pub port: u16,
    pub path: String,
    pub schemes: EnumSet<Scheme>,
    // TODO: tls_keys
}

/// Main interface to setup endpoints and services.
/// Tries to use the least amount of endpoints required
/// to require fewer socket bindings for `Server`.
#[derive(Deserialize, Clone, Debug, Default)]
pub struct EndpointsServices(pub(crate) Arc<DashMap<Endpoint, Services>>);

impl EndpointsServices {
    pub fn with_all_services_on_single_endpoint(endpoint: Endpoint) -> Self {
        let mut endpoints_services = Arc::new(DashMap::new());
        endpoints_services.insert(endpoint, Services::all());
        Self(endpoints_services)
    }

    pub fn insert(&self, endpoint: Endpoint, service: Service) {
        todo!()
    }

    /// Consolidates services and endpoints to use the minimum amount of endpoints required for binding and serving.
    pub fn consolidate(&self) {
        todo!()
    }

    /// Returns a list of served services.
    pub fn services(&self) -> Services {
        let mut unique_services = Services::new();

        for maybe_redundant_services in self.0.iter() {
            for maybe_redundant_service in maybe_redundant_services.iter() {
                unique_services.insert(maybe_redundant_service.clone());
            }
        }

        unique_services
    }

    /// Checks whether all services are covered.
    pub fn covers_all_services(&self) -> bool {
        let required_services = Services::all();

        for served_service in self.services() {
            if !required_services.contains(served_service) {
                return false;
            }
        }

        return true;
    }
}

/// Collection of unique services to maybe unique endpoints.
pub type ServicesEndpoints = DashMap<Service, Arc<Endpoint>>;

impl From<&ServicesEndpoints> for EndpointsServices {
    fn from(service_to_endpoints: &ServicesEndpoints) -> Self {
        todo!()
    }
}

impl Endpoint {
    pub fn new<'s>(socket_addr: SocketAddr, port: u16, path: &'s str, schemes: Schemes) -> Self {
        Self {
            socket_addr,
            port,
            path: path.to_string(),
            schemes,
        }
    }
}

impl FromStr for Endpoint {
    type Err = Error;

    /// Parse `Endpoint` from URL as `&str`.
    // TODO: Change into `Result` and provide typed errors.
    fn from_str<'s>(url: &'s str) -> Result<Self, Self::Err> {
        // TODO: parse correctly schemes
        // TODO: parse as Scheme
        // BUG: expect bug if URI starts with a different scheme
        let mut url = String::from(url);
        if !url.starts_with("https://") && !url.starts_with("http://") {
            // TODO: default to https:// if keys are available
            url = "http://".to_string() + &url.to_string();
        }
        // TODO: parse schemes instead
        let schemes = schemes_default_secure();

        let endpoint = Url::parse(&url).expect("Endpoint address is incorrect. Requires bind socket address (IP:PORT) followed by base path. Correct example: ENDPOINT_ALL=\"domain:4000/route\"");
        let host = endpoint
            .host()
            .expect("Endpoint address is incorrect. Requires bind socket address (IP:PORT).");
        let ip: Option<IpAddr> = match host {
            Ipv4(ip) => Some(IpAddr::V4(ip)),
            Ipv6(ip) => Some(IpAddr::V6(ip)),
            _ => None,
        };
        let ip = ip.expect("TODO: error ip required");
        let port = endpoint.port().expect("TODO: error expected port.");
        let path = String::from(endpoint.path());
        let socket_addr = SocketAddr::from((ip, port));

        Ok(Self {
            socket_addr,
            port,
            path,
            schemes,
        })
    }
}
