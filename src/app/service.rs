//! Types of services.

use enum_iterator::IntoEnumIterator;
use enumset::{EnumSet, EnumSetType};
use parse_display::{Display, FromStr};
use serde::Deserialize;
use std::collections::HashSet;

use super::App;

/// Many of `Service` can be provided on `EndpointsServices`
#[rustfmt::skip]
#[derive(Deserialize, Display, FromStr, Debug, IntoEnumIterator, EnumSetType, Hash)]
pub enum Service {
    Public,
    Editor,
    // Analysis,
    // Trends,
    // Auth,
    Settings,
    // Translations,
}

/// Set of services
pub type Services = EnumSet<Service>;

//pub type ServiceFn = Box<dyn HyperService<Request<Service>>>;

//use tower_layer::Layer;
//use futures::FutureExt;
use std::error::Error;
use std::fmt;
use std::future::Future;
use std::pin::Pin;
use std::task::{Context, Poll};
use std::time::Duration;
