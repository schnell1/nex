//! Parse and serve `App`.
#![allow(
    unused_imports,
    dead_code,
    unused_mut,
    unused_variables,
    unused_must_use,
    unreachable_code
)]

pub mod definition;
pub mod endpoint;
pub mod runtime_config;
pub mod server;
pub mod service;

pub use crate::data::{self, Data};

pub use self::{definition::Definition, runtime_config::RuntimeConfig};

pub(crate) use self::{
    endpoint::{Endpoint, EndpointsServices, ServicesEndpoints},
    server::Server,
    service::{Service, Services},
};

use std::{
    convert::TryFrom,
    default::default,
    sync::{Arc, RwLock, Weak},
    time::Duration,
};

use thiserror;
/// Errors than can occur during configuration, setup, building, running.
#[derive(thiserror::Error, Debug)]
#[non_exhaustive]
pub enum Error {
    #[error(transparent)]
    Definition(#[from] definition::Error),
    #[error(transparent)]
    RuntimeConfig(#[from] runtime_config::Error),
    #[error(transparent)]
    Server(#[from] server::Error),
    #[error(transparent)]
    Endpoint(#[from] endpoint::Error),
    #[error(transparent)]
    Data(#[from] data::Error),
}

pub type AppResult = Result<(), Error>;

/// Main interface to setup data, build app and run app.
/// Constructs itself behind a pointer as `Arc<App>` in order to be shared with interior mutability.
#[derive(Clone, Debug)]
pub struct App {
    pub definition: Arc<RwLock<Definition>>,
    pub data: Arc<Data>,
    // Uninitialized because we try to setup database first.
    pub server: Arc<RwLock<Option<Server>>>,
    pub runtime_config: Arc<RwLock<RuntimeConfig>>,
    pub endpoints_services: Arc<RwLock<EndpointsServices>>,
}

impl TryFrom<(Definition, RuntimeConfig)> for App {
    type Error = Error;

    /// Construct `App` from tuple `(Definition, RuntimeConfig)`.
    fn try_from(dr: (Definition, RuntimeConfig)) -> Result<Self, Self::Error> {
        todo!()
    }
}

impl App {
    pub fn try_from_files<S: AsRef<str>>(
        definition_file_path: S,
        dotenv_file_path: S,
    ) -> Result<Arc<Self>, Error> {
        let definition = Definition::try_from_file(definition_file_path)?;
        let runtime_config = RuntimeConfig::try_from_file(dotenv_file_path)?;
        let endpoints_services = Arc::new(RwLock::new(EndpointsServices::from(&runtime_config)));

        Ok(Arc::new(Self {
            definition: Arc::new(RwLock::new(definition)),
            data: default(),
            runtime_config: Arc::new(RwLock::new(runtime_config)),
            endpoints_services,
            server: default(),
        }))
    }

    /// Sets refresh interval as caching strategy for data.
    pub fn data_refresh_with_interval(&self, interval: Duration) {
        todo!()
    }

    /// Check if data is setup correctly.
    pub fn data_check(&self) -> Result<(), Error> {
        todo!()
    }

    /// Setup data.
    pub fn data_setup(&self) -> Result<(), Error> {
        todo!()
    }

    /// Migrate data required from changes to app definition.
    pub fn data_migrate(&self) -> Result<(), Error> {
        todo!()
    }

    /// Check app definition and runtime configuration.
    pub fn app_check(&self) -> Result<(), Error> {
        todo!()
    }

    /// Build app from definition.
    pub fn app_build(&self) -> Result<(), Error> {
        todo!()
    }

    /// Run app.
    pub fn app_run(&self) -> Result<(), Error> {
        todo!()
    }

    /// Run app and live-reload changes to app definition.
    pub fn app_dev(&self) -> Result<(), Error> {
        todo!()
    }
}
