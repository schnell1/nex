#![feature(default_free_fn)]
//! nex migt become a CMS
#![allow(
    unused_imports,
    dead_code,
    //unused_mut,
    unused_variables,
    //unused_must_use,
    //unreachable_code
)]

pub mod app;
pub mod data;
pub(crate) mod roles;

pub use crate::{
    app::{App, AppResult, Definition},
    data::Data,
};

pub(crate) use roles::Roles;

/// "Precise" diagnostics of types.
/// Gets the type name of anything.
#[allow(dead_code)]
pub(crate) fn type_name<T>(anything: T) -> &'static str {
    let mut name = std::any::type_name::<T>();

    // Maybe remove leading "&".
    if name.starts_with("&") {
        name = &name[1..];
    }

    name
}
