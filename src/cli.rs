//! CLI to run and serve app.

#![allow(
    unused_imports,
    dead_code,
    unused_mut,
    unused_variables,
    unused_must_use,
    unreachable_code
)]
#![feature(default_free_fn)]
#![feature(trait_alias)]
#![feature(associated_type_bounds)]
#![feature(duration_constants)]

use nex::{app, App, Data, Definition};
use pico_args;
use tokio;

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // TODO: rewrite using pico-args
    /*
    use clap::{App as Cmd, AppSettings as CmdSettings, Arg};
    let nex_toml = || Arg::new("nex.toml");
    let cli = Cmd::new("nex")
        .version("V0.0.0.0.0.0.0.0.17,3° ※")
        .author("∭  übelst schnell | nex might become a CMS generator.")
        //.about("")
        .setting(CmdSettings::ColoredHelp)
        .subcommand(Cmd::new("check").about("Check app definition, additional app code, runtime configuration, database access, database setup").arg(nex_toml()))
        .subcommand(Cmd::new("db")
        .about("Manage database")
        .subcommand(Cmd::new("setup").about("Setup database from file `nex.toml`").arg(nex_toml()))
        .subcommand(Cmd::new("check").about("Check database access, database setup").arg(nex_toml())))
        .subcommand(Cmd::new("app")
        .about("Build and run the app from file `nex.toml`")
        .subcommand(Cmd::new("run").about("Build and run app. You might want to call `nex db setup` beforehand").arg(nex_toml()))
        .subcommand(Cmd::new("check").about("Check app definition, additional app code, runtime configuration").arg(nex_toml()))
        .subcommand(Cmd::new("build").about("Build app").arg(nex_toml()))
        //.subcommand(Cmd::new("dev").about("Live-reload app while developing").arg(nex_toml()))
    ).get_matches();
    */
    // TODO: dispatch cli

    // Parse files `.env` and `nex.toml` to create `App`.
    //let app = App::try_from_env_and_definition_file("nex.toml")?;
    // Setup database from `Definition`
    //let data = Data::try_setup_from_app(&app)?;

    // Overwrite refresh interval for data.
    //let seconds = std::time::Duration::SECOND * 30;
    //data.set_refresh_interval(seconds);

    // Run server
    //Server::from((app, data));

    use dashmap::DashMap;
    use nex::data::{
        cache::{Refresh, Refresher},
        query::SelectAll,
    };
    use std::time::Duration;

    let seconds = Duration::from_secs(5);
    let d = DashMap::new();
    d.insert(17u32, 5u32);

    // TODO: parse nex.toml
    /*
    let nex_definition = AppDefinition::from_file("examples/nutri/nex");
    dbg!(nex_definition);
    */

    // TODO: compile app_name.rs
    // TODO: pretty print errors and show help as verbose and beautiful as e.g. cargo

    Ok(())
}
