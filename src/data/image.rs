//! Handling of images linked from database to mounted file storage.

use super::Texts;
use serde::Deserialize;

// TODO
#[derive(Clone, Debug, Deserialize)]
pub struct Images {
    pub texts: Option<Texts>,
}

// TODO
#[derive(Clone, Debug, Deserialize)]
pub struct Image {}
