//! Queries returning views of items, IDs or collections.
//! Performed against database and file storage.
//! Execution can involve heavy computation and remote fetching.

use super::{HasId, HasCollection, HasDeps, Setup};
use async_trait::async_trait;
use super::Error;

pub trait Queryable: Send + Sync + HasId + HasCollection + HasDeps + Setup {}

/// Get a view of all elements.
#[async_trait]
pub trait SelectAll: Queryable {
    type Error;

    /// Returns all elements as a collection of `T`.
    async fn select_all(deps: Self::Deps) -> Result<Self::C, <Self as SelectAll>::Error>;
}
