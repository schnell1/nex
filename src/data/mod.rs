//! Managing access to `Data` in database and directories.

pub mod cache;
pub mod db;
pub mod dir;
pub mod locale;
// TODO: pub mod slugs;
pub mod image;
pub mod item;
pub mod profile;
pub mod query;
pub mod taxonomy;
pub mod text;

pub use self::{
    cache::Refresh,
    db::DBPool,
    dir::Dir,
    image::{Image, Images},
    item::{Item, Items},
    locale::{Locale, LocaleCode, Locales},
    profile::{Profile, Profiles},
    // TODO: slugs::{Slug, Slugs},
    taxonomy::{Taxonomies, Taxonomy},
    text::{Text, Texts},
};

pub(super) use self::{cache::Refresher, db::DB, query::SelectAll};
use crate::{type_name, Definition};

use std::{
    any::Any,
    convert::TryFrom,
    hash::Hash,
    path::Path,
    sync::{Arc, RwLock},
    time::Duration,
};
use async_trait::async_trait;
pub use bigdecimal::BigDecimal;
pub use chrono::naive::NaiveDateTime as DateTimeUtc;
use chrono::{naive::NaiveDateTime, offset::Utc};
use dashmap::DashMap;
pub use uuid::Uuid;

use thiserror;
/// Errors from database and files
#[derive(thiserror::Error, Debug)]
#[non_exhaustive]
pub enum Error {
    #[error("Setup not applied for: `{type_name}`")]
    SetupNotAppliedFor { type_name: String },
    #[error(transparent)]
    Dir(#[from] dir::Error),
    #[error(transparent)]
    Sqlx(#[from] sqlx::error::Error),
    #[error(transparent)]
    Cache(#[from] cache::Error),
    #[error("`Data::try_from(definition)` failed: {0}")]
    TryFromDefinitionFailed(String),
}

impl Error {
    pub(super) fn setup_not_applied_for<T>(anything: T) -> Self {
        Self::SetupNotAppliedFor {
            type_name: type_name(anything).into(),
        }
    }
}

/// Manages access to application data in database and directories. Use .clone() to get a cheap handle (internally using `Arc`).
#[derive(Clone, Debug, Default)]
pub struct Data {
    pub db_pool: Arc<RwLock<Option<DBPool>>>,
    pub dir_pool: Arc<RwLock<Option<Dir>>>,
    // TODO: pub locales: ,
    // TODO: pub profiles: ,
    // TODO: pub slugs: ,
    // TODO: pub items: ,
    // TODO: pub taxononomies: ,
    // TODO: pub images: ,
}

impl Data {
    /// Tries to setup data and file storage.
    pub fn try_setup(&self) -> Result<(), Error> {
        todo!()
    }

    /// Connect to database.
    pub fn connect_db<S: AsRef<str>>(uri: S) -> Result<(), Error> {
        todo!()
    }

    /// Sets the given directory as file storage.
    pub fn set_dir<S: AsRef<str>>(dir: S) -> Result<(), Error> {
        todo!()
    }
}

impl TryFrom<&Definition> for Data {
    type Error = Error;

    fn try_from(definition: &Definition) -> Result<Data, Self::Error> {
        todo!()
    }
}

/// Setup dependencies with maybe heavy computation.
#[async_trait]
pub trait Setup: Send + Sync + HasDeps + HasCollection {
    type Error: std::error::Error;

    /// Check if requirements are applied.
    fn is_setup_applied(deps: Self::Deps) -> bool;
    /// Setup requirements, e.g. tables, triggers, functions, constraints, channels.
    async fn apply_setup(deps: Self::Deps) -> Result<(), Self::Error>;
    /// Revert changes.
    async fn revert_setup(deps: Self::Deps) -> Result<(), Self::Error>;
}

/// Implementable for types which can be uniquely identified.
///
/// Inside implementation, use `Self::Id` to inject the id type.
pub trait HasId: Send + Sync {
    type Id: Send + Sync + Hash + Eq;

    fn id(&self) -> Self::Id;
}

/*impl<T, Id> HasId for DashMap<Id, T>
where
    T: Send + Sync,
    Id: Send + Sync + Hash + Eq,
{
    type Id = Id;
}*/

/// Used as a bound for other traits in
/// order to inject runtime dependencies.
///
/// Implementable for types with thread-safe dependencies.
///
/// Inside implementation, use `Self::Deps` to use the dependency type.
///
/// # Example
///
/// ```rust
/// use nex::data::HasDeps;
///
/// trait Access: HasDeps {
/// //            -------
///
///     fn access(deps: Self::Deps) -> Self;
/// //                  ----------
/// }
///
/// struct Knobbsi(u8);
///
/// impl HasDeps for Knobbsi {
///     type Deps = crossbeam::channel::Receiver<u8>;
/// }
///
/// impl Access for Knobbsi {
///     fn access(recv: Self::Deps) -> Knobbsi {
///         Knobbsi(recv.recv().unwrap())
///     }
/// }
///
/// #[test]
/// fn knobbsi_255() {
/// 	let (send, recv) = crossbeam::channel::bounded(1);
/// 	send.send(255);
/// 	assert_eq!(255, Knobbsi::access(recv).0);
/// }
/// ```
pub trait HasDeps: Send + Sync {
    type Deps: Send + Sync;
}

/// Implementable for types that can be an element of a collection.
///
/// Inside implementation, use `Self::C` to inject the collection type.
pub trait HasCollection: Send + Sync {
    type C: Send + Sync + IntoIterator<Item = Self>;
}

/// This is implemented for DashMap's iterator item `(K, V)` as `(Id, T)`.
impl<T, Id> HasCollection for (Id, T)
where
    T: Send + Sync + IntoIterator<Item = Self>,
    Id: Send + Sync + Hash + Eq,
{
    type C = DashMap<Id, T>;
}

/// Gets the current datetime.
pub fn now_utc() -> DateTimeUtc {
    NaiveDateTime::from(Utc::now().naive_utc())
}
