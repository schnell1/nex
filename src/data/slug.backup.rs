// TODO: Rewrite everything. `Slugs` must handle searching/validating entities dynamically.

// TODO: make `Slugs` usable with `Items`
// TODO: make `Slugs` usable with `Profiles`
// TODO: make `Slugs` usable with `Taxonomies`
// TODO: make `Slugs` usable with `Images`

use crate::data::{
    db::{DBPool, DBRow, DB},
    now_utc, DateTimeUtc, Refresh, Uuid, Locale
};
// TODO: remove anyhow
use anyhow::anyhow;
use async_trait::async_trait;
use crossbeam::channel::{bounded, Receiver, Sender};
use dashmap::DashMap;
use serde::Deserialize;
use sqlx::{query_as, FromRow, Row};
use std::{
    fmt::Debug,
    sync::{Arc},
    time::{Duration},
};
use tokio::{
    sync::{mpsc::channel as bounded_future, RwLock as RwLockFuture},
    task::{spawn, JoinHandle},
    time::sleep,
};

pub type SlugKey = String;

/// String-typed index for content entities.
#[derive(Debug)]
pub struct Slugs {
    index: Arc<DashMap<SlugKey, (SlugEntity, Option<Locale>)>>,
    refreshed_at: Arc<RwLockFuture<Option<DateTimeUtc>>>,
    refresh_interval: Duration,
    refresh_handle: Arc<RwLockFuture<Option<JoinHandle<()>>>>,
    refresh_sender: Sender<SlugsState>,
    refresh_receiver: Receiver<SlugsState>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Slug {
    slug: SlugKey,
    entity: SlugEntity,
    locale: Option<Locale>,
}

// TODO: rewrite. decouple id.
#[derive(Copy, Clone, Debug, Deserialize, Hash, Eq, PartialEq)]
pub enum SlugEntity {
    Tag(Uuid),
    Food(Uuid),
    // TODO: Item(Uuid),
    // TODO: Profile(Uuid),
    // TODO: Taxonomy(Uuid),
    // TODO: Locale(LocaleCode),
}

// TODO: rewrite
impl SlugEntity {
    fn from_options(tag_id: Option<Uuid>, food_id: Option<Uuid>) -> anyhow::Result<Self> {
        if tag_id.is_some() {
            Ok(SlugEntity::Tag(tag_id.unwrap()))
        } else if food_id.is_some() {
            Ok(SlugEntity::Food(food_id.unwrap()))
        } else {
            Err(anyhow!("No id is set for either tag or food"))
        }
    }
}

#[derive(Copy, Clone, Debug)]
enum SlugsState {
    Refreshing(Duration),
    Halted,
}

#[async_trait]
impl Refresh for Slugs {
    type Deps = DBPool;

    fn with_refresh_interval(refresh_interval: Duration) -> Self {
        let index = Arc::new(DashMap::new());
        let refresh_handle = Arc::new(RwLockFuture::new(None));
        let refreshed_at = Arc::new(RwLockFuture::new(None));
        let (refresh_sender, refresh_receiver) = bounded(0);
        let refresh_interval = refresh_interval.clone();

        Self {
            index,
            refreshed_at,
            refresh_handle,
            refresh_interval,
            refresh_sender,
            refresh_receiver,
        }
    }

    async fn refresh(&self, deps: Self::Deps) {
        // TODO: instead implement using `triggered` crate
        let refreshed_at = self.refreshed_at();
        let refresh_interval = self.refresh_interval;

        if refreshed_at.is_some() {
            let refreshed_at = refreshed_at.unwrap();
            let now = now_utc();
            let time_elapsed_since_last_refresh = now - refreshed_at;
            let next_refresh = refreshed_at + chrono::Duration::from_std(refresh_interval).unwrap();
            if now > next_refresh {
                self.restart_refreshing(deps).await;
            } else {
                let _ = self.refresh_sender
                    .send(SlugsState::Refreshing(refresh_interval));
            }
        } else {
            self.restart_refreshing(deps).await;
        }
    }
}

// TODO: Rewrite. Must handle searching/validating entities dynamically.
impl Slugs {
    pub async fn restart_refreshing<'s>(&self, db_pool: DBPool) {
        let _ = self.stop_refreshing();
        let index = self.index.clone();
        let mut refresh_interval = self.refresh_interval.clone();
        let refresh_receiver = self.refresh_receiver.clone();
        let (first_run_sender, mut first_run_receiver) = bounded_future::<()>(1);

        // scoped to avoid wait locks and to satisfy lifetime rules
        {
            let refreshed_at = self.refreshed_at.clone();

            let refresh_fn = async move {
                let mut first_run = true;
                let mut _index_last_len = 0;

                loop {
                    // TODO: listen asynchronously on events. refresh on demand.
                    if let Ok(state) = refresh_receiver.recv() {
                        match state {
                            SlugsState::Refreshing(interval) => {
                                refresh_interval = interval;
                            }
                            SlugsState::Halted => break,
                        }
                    }

                    let slugs_result =
                        query_as::<DB, Slug>("SELECT slug, food_id, tag_id FROM slugs")
                            .fetch_all(&db_pool)
                            .await;

                    if slugs_result.is_err() {
                        if first_run {
                            // first_run = false;
                            let _ = first_run_sender.send(()).await;
                            let _ = first_run_sender.closed();
                            return ();
                        } else {
                            continue;
                        }
                    }

                    let slugs = slugs_result.unwrap();

                    for slug in slugs {
                        index.insert(slug.slug, slug.entity);
                    }

                    _index_last_len = index.len();

                    // scoped to avoid wait lock
                    {
                        let mut refreshed_at = refreshed_at.write().await;
                        let now = now_utc();
                        *refreshed_at = Some(now);
                    }

                    if first_run {
                        first_run = false;
                        let _ = first_run_sender.send(()).await;
                        let _ = first_run_sender.closed();
                    }

                    sleep(refresh_interval).await;
                }

                return ();
            };

            let mut refresh_handle = self.refresh_handle.write().await;
            *refresh_handle = Some(spawn(refresh_fn));
        }

        let _ = self.refresh_sender
            .send(SlugsState::Refreshing(self.refresh_interval));
        let _ = first_run_receiver.recv().await;
    }

    pub async fn stop_refreshing(&self) -> &Self {
        let (tx, rx): (_, Receiver<()>) = bounded(1);
        {
            let _ = self.refresh_sender.send(SlugsState::Halted);
            let mut refresh_handle = self.refresh_handle.clone();

            spawn(async move {
                let refresh_handle = refresh_handle.write().await;
                let _join_result = refresh_handle.as_mut().unwrap().await;
                refresh_handle.as_ref().unwrap().abort();
                *refresh_handle = None;
                let _ = tx.send(());
            });

            let _ = self.refresh_receiver.try_recv();
        }
        let _ = rx.recv();
        self
    }

    pub fn refreshed_at(&self) -> Option<DateTimeUtc> {
        let (tx, rx): (_, Receiver<Option<DateTimeUtc>>) = bounded(1);

        {
            let refreshed_at = self.refreshed_at.clone();
            spawn(async move {
                let refreshed_at = *refreshed_at.read().await;
                let _ = tx.send(refreshed_at);
            });
        }

        rx.recv().unwrap()
    }

    pub fn get_all(&self) -> Vec<Slug> {
        self.index
            .iter()
            .map(|s| Slug {
                slug: s.key().into(),
                entity: *s.value(),
                // TODO: fetch locale
                locale: None,
            })
            .collect()
    }

    pub fn contains_slug(&self, slug_key: &SlugKey) -> bool {
        self.index.contains_key(slug_key)
    }

    pub fn get(&self, slug_key: &SlugKey) -> Option<Slug> {
        self.index.get(slug_key).map_or(None, |s| {
            Some(Slug {
                slug: s.key().into(),
                entity: *s.value(),
                // TODO: fetch locale
                locale: None,
            })
        })
    }

    pub fn find_slugs(&self, entity: &SlugEntity) -> Vec<SlugKey> {
        self.index
            .iter()
            .filter(|s| s.value() == entity)
            .map(|s| s.key().clone())
            .collect()
    }

    pub fn find_aliases(&self, slug_key: &SlugKey) -> Vec<SlugKey> {
        self.index
            .iter()
            .filter(|s| s.key() == slug_key)
            .map(|s| self.find_slugs(s.value()))
            .next()
            .unwrap_or(Vec::new())
    }
}

impl<'r> FromRow<'r, DBRow> for Slug {
    fn from_row(row: &'r DBRow) -> Result<Self, sqlx::Error> {
        let slug: String = row.try_get("slug")?;
        let food_id: Option<Uuid> = row.try_get("food_id")?;
        let tag_id: Option<Uuid> = row.try_get("tag_id")?;
        let entity = SlugEntity::from_options(tag_id, food_id);

        return match entity {
            Ok(entity) => Ok(Slug { slug, entity }),
            _ => Err(sqlx::Error::Decode(
                "slug must be assigned to exactly one entity (food xor tag)".into(),
            )),
        };
    }
}
