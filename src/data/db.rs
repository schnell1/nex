//! Managing database access.
pub use sqlx::postgres::PgPool as DBPool;
pub(crate) use sqlx::{
    database::{Database, HasArguments},
    postgres::{PgRow as Row, Postgres as DB},
    query::Query,
};

pub fn query(sql: &str) -> Query<'_, DB, <DB as HasArguments<'_>>::Arguments> {
    sqlx::query::<DB>(sql)
}
