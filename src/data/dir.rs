//! Storage and handling of files in directories.

use thiserror;
#[derive(thiserror::Error, Debug)]
#[non_exhaustive]

pub enum Error {
    #[error(transparent)]
    Io(#[from] std::io::Error),
}

// TODO
#[derive(Clone, Debug)]
pub struct Dir {}
