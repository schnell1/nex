//! Localization handling.

#![allow(
    unused_imports,
    dead_code,
    unused_mut,
    unused_variables,
    unused_must_use,
    unreachable_code
)]
use super::{
    db::query, DBPool, DateTimeUtc, Duration, Error, HasCollection, HasDeps, HasId, Setup, Text,
};

use async_trait::async_trait;
use dashmap::DashSet;
use serde::Deserialize;
use sqlx::{pool::PoolConnection, query::QueryAs, query_as, Executor, FromRow, Row};
use std::{
    collections::HashMap,
    hash::{Hash, Hasher},
    sync::{Arc, RwLock},
};
use tokio::sync::RwLock as RwLockFuture;

pub type Locales = DashSet<Locale>;
pub type LocaleCode = String;

#[derive(Clone, Debug, Deserialize, Eq)]
pub struct Locale {
    pub code: LocaleCode,
    pub native_name: String,
}

#[async_trait]
impl Setup for Locale {
    type Error = Error;

    fn is_setup_applied(db_pool: Self::Deps) -> bool {
        todo!()
    }

    async fn apply_setup(db_pool: Self::Deps) -> Result<(), Self::Error> {
        let _ = query(
            "
        CREATE TABLE IF NOT EXISTS locales (
            code VARCHAR(20) NOT NULL PRIMARY KEY,
            native_name TEXT
        );",
        )
        .execute(&*db_pool)
        .await?;
        Ok(())
    }

    async fn revert_setup(db_pool: Self::Deps) -> Result<(), Self::Error> {
        let _ = query("DROP TABLE locales;").execute(&*db_pool).await?;
        Ok(())
    }
}

impl HasId for Locale {
    type Id = LocaleCode;

    fn id(&self) -> Self::Id {
        self.code.clone()
    }
}

impl HasDeps for Locale {
    type Deps = &'static DBPool;
}

impl HasCollection for Locale
where
    Self: HasId,
{
    type C = Locales;
}

impl Hash for Locale {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.code.hash(state);
    }
}

impl PartialEq for Locale {
    fn eq(&self, other: &Self) -> bool {
        self.code == other.code
    }
}
