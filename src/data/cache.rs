//! Caching strategies.
use super::{now_utc, DBPool, DateTimeUtc, HasCollection, HasDeps, HasId, SelectAll};

use std::{hash::Hash, sync::Arc, time::Duration};

use async_trait::async_trait;
use crossbeam::channel::{bounded, Receiver, Sender};
use dashmap::DashMap;
use tokio::{
    sync::{mpsc::channel as bounded_future, RwLock as RwLockFuture},
    task::{spawn, JoinHandle},
    //time::sleep,
};
use triggered::trigger;

use anyhow;
use thiserror;
/// Errors while caching.
#[derive(thiserror::Error, Debug)]
#[non_exhaustive]
pub enum Error {
    #[error("Error while refreshing: {0}")]
    Refreshing(String),
    #[error(transparent)]
    Other(#[from] anyhow::Error),
}

/*/// Makes a collection self-caching and spawns a self-managed caching task.
#[async_trait]
pub trait Cache: Send + Sync + Sized {
    type Cached: Send + Sync;
    type Cacher: Send + Sync;
    type Deps: Deps;
    type Error: std::error::Error + Into<Self::Error>;

    async fn apply_all_cache_strategy(
        self,
        deps: Self::Deps,
    ) -> Result<(Self::Cached, Self::Cacher), Self::Error>;
}*/

/// Makes a collection self-updating by interval and spawns a self-managed updater task.
/// Warrants consistent data but is expensive for local/remote performance and memory.
/// Associated type `Refreshed` shoud to be defined as `Self` behind a shared locked pointer, e.g. `Arc<RwLock<Self>>>` or `Arc<DashSet<Self>>`.
#[async_trait]
pub trait Refresh: Send + Sync + Sized + HasDeps {
    type Refreshed: Send + Sync;
    type Error: std::error::Error;

    /// Starts refreshing with interval and dependencies.
    /// Returns the refreshed collection behind an atomic pointer and a self-managed updater task.
    async fn restart_refreshing(
        self,
        interval: Duration,
        deps: Self::Deps,
    ) -> Result<(Self::Refreshed, Refresher<Self::Refreshed>), Self::Error>;
    /// Refreshes immediately and returns timestamp of completion.
    async fn refresh(refresher: &Refresher<Self::Refreshed>) -> Result<DateTimeUtc, Self::Error>;

    /// Stops refreshing.
    /// Consumes wrapper und updater.
    /// Returns `Self`.
    async fn stop_refreshing(
        refreshed: Self::Refreshed,
        refresher: Refresher<Self::Refreshed>,
    ) -> Self {
        todo!()
    }
}

/// Wrapping type which periodically updates T.
/// To make use of interior mutability, T should be a locked type,
/// e.g. `Mutex<u32>`, `RwLock<String>`, `DashMap<u32, String>`.
#[derive(Clone, Debug)]
pub struct Refresher<T: Send + Sync> {
    pub data: Arc<T>,
    interval: Arc<RwLockFuture<Option<Duration>>>,
    refreshed_at: Arc<RwLockFuture<Option<DateTimeUtc>>>,
    handle: Arc<RwLockFuture<Option<JoinHandle<()>>>>,
    handle_rx: Sender<RefresherState>,
    handle_tx: Receiver<RefresherState>,
}

/// Refresher is either `Halted`, or `Refreshing` with interval and time info about last and next refresh.
#[derive(Debug, Clone)]
pub enum RefresherState {
    /// Currently not refreshing but may have been running before.
    Halted { refreshed_at: Option<DateTimeUtc> },
    /// Holds refresh interval and time info about last and next refreesh.
    Refreshing {
        interval: Duration,
        refreshed_at: DateTimeUtc,
        next_refresh_at: DateTimeUtc,
    },
}

impl Default for RefresherState {
    /// Default state is `Halted`.
    fn default() -> Self {
        Self::Halted { refreshed_at: None }
    }
}

impl<T: Send + Sync> Refresher<T> {
    pub fn set_refreshed_now(&self) {
        let refreshed_at = self.refreshed_at.clone();

        let (set_done, get_done) = trigger();
        spawn(async move {
            *refreshed_at.write().await = Some(now_utc());
            set_done.trigger();
        });
        get_done.wait();
    }

    pub fn refreshed_at(&self) -> Option<DateTimeUtc> {
        let refreshed_at = self.refreshed_at.clone();

        let (tx, rx) = bounded(1);
        spawn(async move {
            let _ = tx.send(*refreshed_at.read().await);
        });
        rx.recv().unwrap()
    }

    pub fn set_interval(&self, interval: Duration) {
        let refresher_interval = self.interval.clone();

        let (tx, rx): (Sender<()>, _) = bounded(1);
        spawn(async move {
            *refresher_interval.write().await = Some(interval);
            let _ = tx.send(());
        });
        rx.recv().unwrap();
    }
}

impl<T, Id> From<DashMap<Id, T>> for Refresher<DashMap<Id, T>>
where
    T: Send + Sync,
    Id: Send + Sync + Hash + Eq,
{
    fn from(dashmap: DashMap<Id, T>) -> Refresher<DashMap<Id, T>> {
        let (handle_rx, handle_tx) = bounded(0);

        Refresher {
            data: Arc::new(dashmap),
            interval: Arc::new(RwLockFuture::new(None)),
            refreshed_at: Arc::new(RwLockFuture::new(None)),
            handle: Arc::new(RwLockFuture::new(None)),
            handle_tx,
            handle_rx,
        }
    }
}

pub struct RefreshDepsDB {
    pub db_pool: DBPool,
    pub interval: Duration,
}

impl From<RefreshDepsDB> for DBPool {
    fn from(deps: RefreshDepsDB) -> Self {
        deps.db_pool
    }
}

/// Refresh and collect into `Arc<DashMap<Id, T>>`.
#[async_trait]
impl<T, Id> Refresh for T
where
    Self: Send + Sync + SelectAll + HasId<Id = Id> + HasDeps + HasCollection,
    Self: HasId<Id = Id>,
    Id: Send + Sync + Hash + Eq,
    Error: From<<Self as SelectAll>::Error>,
{
    type Refreshed = Arc<DashMap<<Self as HasId>::Id, Self>>;
    type Error = Error;

    async fn restart_refreshing(
        self,
        interval: Duration,
        deps: Self::Deps,
    ) -> Result<(Self::Refreshed, Refresher<Self::Refreshed>), Self::Error> {
        let all = T::select_all(deps).await?.into_iter();
        todo!()
    }

    async fn refresh(refresher: &Refresher<Self::Refreshed>) -> Result<DateTimeUtc, Self::Error> {
        todo!()
    }
}

// TODO: Implement this. Conflicts with implementation above for `T` and `Id`.
//       error[E0119]: conflicting implementations of trait `data::cache::Refresh`
/*/// Refresh and collect into `Arc<DashSet<T>>`.
#[async_trait]
impl<T> Refresh for T
where
    Self: Send + Sync + SelectAll + HasDeps + HasCollection,
    Self: Hash + Eq,
    Error: From<<Self as SelectAll>::Error>,
{
    type Refreshed = Arc<DashSet<T>>;
    type Error = Error;

    async fn restart_refreshing(
        self,
        interval: Duration,
        deps: Self::Deps,
    ) -> Result<(Self::Refreshed, Refresher<Self::Refreshed>), Self::Error> {
        let all = T::select_all(deps).await?.into_iter();
        todo!()
    }

    async fn refresh(refresher: &Refresher<Self::Refreshed>) -> Result<DateTimeUtc, Self::Error> {
        todo!()
    }
}*/
