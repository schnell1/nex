//! Texts and their localization handling.

use super::Locale;

use std::{
    collections::{HashMap, HashSet},
    sync::Weak,
};

use serde::Deserialize;

pub type Text = String;

/*
/// Sum type of texts with or without translations
// TODO: rewrite
#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
pub enum Text {
    /// Single text unassigned to any locale
    OneText(String),
    /// Many texts unassiagned to any locale
    ManyTexts(HashSet<String>),
    /// One text assigned per one locale
    OneTextPerLocale(HashMap<Locale, String>),
    /// Many texts assigned per many locales
    ManyTextsPerLocale(MultiMap<Locale, String>),
}*/

// TODO
#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
pub struct Texts;
